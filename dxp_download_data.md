Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s2-0034-placer-hr).

|document|download options|
|:-----|-----:|
|operating manual           |[de](https://gitlab.com/ourplant.net/products/s2-0034-placer-hr/-/raw/main/01_operating_manual/S2-0034_A_BA_Placer.pdf) / [en](https://gitlab.com/ourplant.net/products/s2-0034-placer-hr/-/raw/main/01_operating_manual/S2-0034_A_OM_Placer.pdf)|
|assembly drawing           |[de](https://gitlab.com/ourplant.net/products/s2-0034-placer-hr/-/raw/main/02_assembly_drawing/s2-0034_C_ZNB_placer.pdf)|
|circuit diagram            |[de](https://gitlab.com/ourplant.net/products/s2-0034-placer-hr/-/raw/main/03_circuit_diagram/S2-0034_C_EPLAN_Placer.pdf)|
|maintenance instructions   |[de](https://gitlab.com/ourplant.net/products/s2-0034-placer-hr/-/raw/main/04_maintenance_instructions/S2-0034_B_WA_Placer.pdf) / [en](https://gitlab.com/ourplant.net/products/s2-0034-placer-hr/-/raw/main/04_maintenance_instructions/S2-0034_A_MI_Placer.pdf)|
|spare parts                |[de](https://gitlab.com/ourplant.net/products/s2-0034-placer-hr/-/raw/main/05_spare_parts/S2-0034_A_EVL_Placer.pdf) / [en](https://gitlab.com/ourplant.net/products/s2-0034-placer-hr/-/raw/main/05_spare_parts/S2-0034_A_SWP_Placer.pdf)|

<!-- 2021 (C) Häcker Automation GmbH -->
